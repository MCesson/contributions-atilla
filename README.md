# Contributions [ATILLA](https://www.atilla.org)

Undermentioned projects are maintained on a self-hosted GitLab instance. 
This is the reason why I refer to them as follows.

## atilla.org

Git repository for https://www.atilla.org.

*https://gitlab.atilla.org/atilla/atilla*

## ATILLA Discord Bot

Bot to handle the ATILLA Discord server.

*https://gitlab.atilla.org/moraujosse/atilla-discord-bot*
